class StationsController < ApplicationController
  def new
    
  end
  def create
  #	 render plain: params[:fuelid].inspect
	params.permit!
 	@station = Station.new(params[:station])

 if	@station.save
  fuels = Fuel.find params[:fuel_id]
  @station.fuels=fuels
 	redirect_to @station
 else
  render :new
 end
end
def show
  		@station = Station.find(params[:id])
end

def index
  @stations = Station.all
end
def edit  
  @station = Station.find(params[:id])
#render plain: params[:id].inspect
end
def update
  @station = Station.find(params[:id])
 
  if @station.update(station_params)
    redirect_to @station
  else
    render 'edit'
  end
end

private
  def station_params
    params.require(:station).permit(:station_number, :social_reason,:address , :zip_code ,:state_id, :municipality_id, :siic, :colony, :phone, :email, :vpm, :cualli, :latitude, :longitude, :organization_id )
 end

end
