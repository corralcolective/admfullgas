class Api::RsController < ApplicationController
	respond_to :json
	#PER_PAGES__RECORD =9
	def index
		#municipalities_paginated = Municipality.order('id').page(params[:page]).per(PER_PAGES__RECORD)
		@municipios= Municipality.where(state: params[:state])  if params[:state].present?
		
		respond_with @municipios
	end
end