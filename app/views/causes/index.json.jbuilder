json.array!(@causes) do |cause|
  json.extract! cause, :id, :clave, :descrition, :cause, :technique_description
  json.url cause_url(cause, format: :json)
end
