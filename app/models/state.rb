class State < ActiveRecord::Base
  has_many :stations
  has_many :municipalities
end
