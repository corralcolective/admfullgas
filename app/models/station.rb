class Station < ActiveRecord::Base
  belongs_to :state
  has_many :verification
  belongs_to :organization
 has_and_belongs_to_many :fuels
   accepts_nested_attributes_for :state,:verification, :organization #, :fuels
end 
