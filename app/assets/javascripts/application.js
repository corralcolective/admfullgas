// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require underscore
//= require gmaps/google
//= require jquery_ujs
//= require turbolinks
//= require_tree .

 $(document).ready(function() {
        $('#station_state_id').on('change', function() {
		 
		 $.get( "https://safe-anchorage-9000.herokuapp.com/api/rs.json",{
		 	state:this.value
		 }, function(data,status){
    	
    	   $('#station_municipality_id').empty();
    	  $.each(data,function(key, value) 
			{
				
			    $('#station_municipality_id').append('<option value=' + value.id + '>' + value.name + '</option>');
			});
   			 })
			.fail(function() {
			alert( "erro DE MENOS" );
			});
		});

		 handler = Gmaps.build('Google');    // map ini
handler.buildMap({ internal: {id: 'map'} }, function(){

  if(navigator.geolocation)
    navigator.geolocation.getCurrentPosition(displayOnMap);
});

function displayOnMap(position){
  var marker = handler.addMarker({
    lat: position.coords.latitude,
    lng: position.coords.longitude,
    "picture": {
        "url": "https://addons.cdn.mozilla.net/img/uploads/addon_icons/13/13028-64.png",
        "width":  36,
        "height": 36
      }
  
  });
  handler.map.centerOn(marker);
};

  var markerOnMap;

  function placeMarker(location) {    // simply method for put new marker on map
    if (markerOnMap) {
    	console.log(location);
      markerOnMap.setPosition(location);
    }
    else {
      markerOnMap = new google.maps.Marker({
        position: location,
        map: handler.getMap()
      });
    }
  }

  google.maps.event.addListener(handler.getMap(), 'click', function(event) {    // event for click-put marker on map and pass coordinates to hidden fields in form
    placeMarker(event.latLng);

   
    
    document.getElementById("station_latitude").value = event.latLng.lat();
     document.getElementById("station_longitude").value = event.latLng.lng();
   
  });

		
    });