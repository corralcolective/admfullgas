# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141116055853) do

  create_table "causes", force: true do |t|
    t.string   "clave"
    t.text     "descrition"
    t.string   "cause"
    t.text     "technique_description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fuels", force: true do |t|
    t.string   "name"
    t.text     "descrition"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fuels_stations", force: true do |t|
    t.integer "station_id"
    t.integer "fuel_id"
  end

  create_table "immobilizations", force: true do |t|
    t.integer "verification_id"
    t.integer "cause"
  end

  create_table "municipalities", force: true do |t|
    t.integer  "state_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "organizations", force: true do |t|
    t.string "name"
  end

  create_table "states", force: true do |t|
    t.string "name"
  end

  create_table "stations", force: true do |t|
    t.string  "station_number"
    t.string  "social_reason"
    t.string  "address"
    t.string  "zip_code"
    t.integer "state_id"
    t.string  "siic"
    t.string  "colony"
    t.string  "phone"
    t.string  "email"
    t.boolean "vpm"
    t.boolean "cualli"
    t.string  "latitude"
    t.string  "longitude"
    t.integer "organization_id"
    t.integer "municipality_id"
    t.integer "fuel_id"
  end

  create_table "verifications", force: true do |t|
    t.integer "station_id"
    t.date    "date"
    t.integer "pipes"
    t.integer "immobilized"
  end

end
