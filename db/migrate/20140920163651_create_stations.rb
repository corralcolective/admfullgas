class CreateStations < ActiveRecord::Migration
  def change
    create_table :stations do |t|
      t.string :station_number
      t.string :social_reason
      t.string :address
      t.string :zip_code
    end
  end
end
