class CreateVerification < ActiveRecord::Migration
  def change
    create_table :verifications do |t|
      t.integer :station_id
      t.date    :date
      t.integer :pipes
      t.integer :immobilized
    end
  end
end
