class ChangeNameTableFuelsStations < ActiveRecord::Migration
 def self.up
    rename_table :stations_fuels, :fuels_stations
  end 
  def self.down
    rename_table :fuels_stations, :stations_fuels
  end
end
