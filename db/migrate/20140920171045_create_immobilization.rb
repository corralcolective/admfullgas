class CreateImmobilization < ActiveRecord::Migration
  def change
    create_table :immobilizations do |t|
      t.integer :verification_id
      t.integer :cause
    end
  end
end
