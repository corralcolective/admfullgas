class AddFieldsStations < ActiveRecord::Migration
  def change
    add_column :stations, :municipality, :string
    add_column :stations, :siic, :string
    add_column :stations, :colony, :string
    add_column :stations, :phone, :string
    add_column :stations, :email, :string
    add_column :stations, :vpm, :boolean
    add_column :stations, :cualli, :boolean
    add_column :stations, :latitude, :string
    add_column :stations, :longitude, :string
    add_column :stations, :organization_id, :integer
  end
end
