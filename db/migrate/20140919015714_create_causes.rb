class CreateCauses < ActiveRecord::Migration
  def change
    create_table :causes do |t|
      t.string :clave
      t.text :descrition
      t.string :cause
      t.text :technique_description

      t.timestamps
    end
  end
end
