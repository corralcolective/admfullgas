class CreateMunicipalities < ActiveRecord::Migration
  def change
    create_table :municipalities do |t|
      t.integer :state_id
      t.string :name

      t.timestamps
    end
  end
end
