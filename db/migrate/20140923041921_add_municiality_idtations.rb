class AddMunicialityIdtations < ActiveRecord::Migration
  def change
    remove_column :stations, :municipality, :string
    add_column :stations, :municipality_id, :integer
  end
end
