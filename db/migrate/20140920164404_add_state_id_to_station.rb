class AddStateIdToStation < ActiveRecord::Migration
  def change
    add_column :stations, :state_id, :integer
  end
end
